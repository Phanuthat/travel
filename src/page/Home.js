import { Container } from '@mui/material';
import React from 'react';
import Banner from '../component/Banner/Banner';
import NewPlace from '../component/NewPlace/NewPlace';
import PopularPlace from '../component/PopularPlace/PopularPlace';

export default function Home() {
  return (
    <>
      <Banner />
      <Container>
        <PopularPlace />
        <NewPlace />
      </Container>
    </>
  );
}
