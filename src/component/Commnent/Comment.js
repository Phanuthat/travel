import { Comment as Comments, List } from 'antd';
import React from 'react';

const Commnet = ({ comments, name }) => (
  <List
    className='comment-list'
    header={`${comments.length} replies`}
    itemLayout='horizontal'
    dataSource={comments}
    renderItem={(item) => (
      <li>
        <Comments
          author={item.name}
          avatar={'https://joeschmoe.io/api/v1/random'}
          content={item}
        />
      </li>
    )}
  />
);

export default Commnet;
