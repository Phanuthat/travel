import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Grid, Rating } from '@mui/material';
import PlaceIcon from '@mui/icons-material/Place';
export default function MediaCard({
  src = '',
  title = '',
  rate = '',
  location = '',
}) {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardMedia component='img' height='250' image={src} alt='green iguana' />
      <CardContent>
        <Grid container spacing={2} justifyContent='space-between'>
          <Grid item>
            <Typography gutterBottom variant='h5' component='div'>
              {title}
            </Typography>
          </Grid>
          <Grid item>
            <Rating name='read-only' value={rate} readOnly size='small' />
          </Grid>
        </Grid>
      </CardContent>
      <CardActions disableSpacing>
        <Button size='small'>
          <PlaceIcon /> {location}
        </Button>
      </CardActions>
    </Card>
  );
}
