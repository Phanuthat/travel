import React from 'react';

import { CircularProgress, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  grid: {
    padding: 10,
  },

  progress: {
    margin: theme.spacing(2),
  },
}));

export default function Loading() {
  const classes = useStyles();

  return (
    <Grid
      item
      xs
      container
      direction='column'
      alignItems='center'
      className={classes.grid}
    >
      <CircularProgress className={classes.progress} />
    </Grid>
  );
}
