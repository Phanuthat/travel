import React from 'react';
import { makeStyles } from '@mui/styles';
import { Container, Grid, Typography } from '@mui/material';

import Post from './Post/Post';
import Insurance from '../Insulan/Insurance';
import Hotel from '../Hotel/Hotel';
const componentStyles = (theme) => ({
  container: {
    paddingTop: '2rem',
    paddingBottom: '4rem',
  },
  imgContainer: {
    objectFit: 'cover',
    width: '100%',
    height: '350px',
  },
  scrollBar: {
    overflowY: 'scroll',
    height: '80rem',
    padding: 10,
  },
  gridContainer: {
    marginTop: '2rem',
  },
});

const useStyles = makeStyles(componentStyles);

export default function Community() {
  const classes = useStyles();

  return (
    <Container className={classes.container} maxWidth='xl'>
      <Typography variant='h3' gutterBottom component='div'>
        Travel Community
      </Typography>
      <Grid container spacing={2} className={classes.gridContainer}>
        <Grid container item xs={12} lg={9} spacing={2}>
          <Grid item xs={12}>
            <div className={classes.scrollBar}>
              <Post />
            </div>
          </Grid>
        </Grid>
        <Grid container item xs={12} lg={3} spacing={2}>
          <Grid item xs={12}>
            <Insurance />
          </Grid>
          <Grid item xs={12}>
            <Hotel />
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
}
