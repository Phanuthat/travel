import React, { useEffect, useState } from 'react';
import { makeStyles } from '@mui/styles';
import { Grid, Paper, TextField, Typography, Button } from '@mui/material';
import { Avatar } from 'antd';
import FolderIcon from '@mui/icons-material/Folder';
import Commnet from '../../Commnent/Comment';
import { imgs } from '../../../image/image';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import {
  getPostList,
  getPostWithCommet,
  insertComment,
  insertPost,
  insertPost2,
} from '../../../firestore/community';
import Loading from '../../Loading/Loading';
const componentStyles = (theme) => ({
  container: {
    paddingTop: '2rem',
    paddingBottom: '2rem',
  },
  imgContainer: {
    objectFit: 'cover',
    width: '100%',
    height: '350px',
  },
});

const useStyles = makeStyles(componentStyles);
export default function Post() {
  const classes = useStyles();

  const [postList, setPostList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [newComment, setNewComment] = useState('');
  const [fetchPost, setIsFetchPost] = useState(false);

  useEffect(() => {
    const call = async () => {
      setIsLoading(true);
      const postList = await getPostList();
      if (postList) {
        setPostList(postList);
        setIsLoading(false);
        setIsFetchPost(false);
      }
    };
    call();
  }, [fetchPost]);

  const addComment = async (id, comments) => {
    const newCommentList = [...comments, newComment];
    const addNewComment = await insertComment(id, newCommentList);
    if (!addNewComment) {
      setIsFetchPost(true);
    }
  };
  return (
    <div>
      {isLoading ? (
        <Loading />
      ) : (
        postList.map((p, i) => {
          return (
            <Paper
              style={{ padding: '40px 20px', marginBottom: 20 }}
              elevation={3}
            >
              <Grid container wrap='nowrap' spacing={2} direction='column'>
                <Grid item></Grid>
                <Grid item xs={12}>
                  <img
                    src={p.img}
                    className={classes.imgContainer}
                    alt='post-img'
                  />
                </Grid>
                <Grid
                  justifyContent='left'
                  item
                  xs
                  zeroMinWidth
                  container
                  spacing={2}
                >
                  <Grid item>
                    <Avatar src='https://joeschmoe.io/api/v1/random'></Avatar>
                  </Grid>
                  <Grid item>
                    <Typography>{p.creatorName}</Typography>
                  </Grid>
                  <Grid item>
                    <Typography>{p.desc}</Typography>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    container
                    direction='row'
                    whiteSpace={2}
                    rowSpacing={2}
                    rowGap={2}
                    columnSpacing={2}
                  >
                    <Grid item>
                      <ThumbUpIcon></ThumbUpIcon>
                    </Grid>
                    <Grid item>
                      <Typography>{p.like}</Typography>
                    </Grid>
                    <Grid item>
                      <Typography>Like</Typography>
                    </Grid>
                  </Grid>
                  {/* <Grid item>
                    <p style={{ textAlign: 'left', color: 'gray' }}>
                      posted 1 minute ago
                    </p>
                  </Grid> */}

                  <Grid item>
                    <Commnet comments={p.comments} name={p.creatorName} />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      id='outlined-multiline-flexible'
                      label='Commnet'
                      multiline
                      maxRows={4}
                      fullWidth
                      onChange={(e) => setNewComment(e.target.value)}
                      value={newComment}
                    />
                  </Grid>
                  <Grid
                    item
                    justifyContent={'right'}
                    container
                    direction={'row'}
                  >
                    <Button
                      variant='contained'
                      onClick={() => addComment(p.id, p.comments)}
                    >
                      Add Comment
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          );
        })
      )}
    </div>
  );
}
