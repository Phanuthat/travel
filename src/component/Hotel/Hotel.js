import {
  Alert,
  AlertTitle,
  Card,
  CardContent,
  CardHeader,
  Grid,
} from '@mui/material';
import React from 'react';
import { imgs } from '../../image/image';
import MediaCard from '../Card/Card';

export default function Hotel() {
  return (
    <Card square>
      <CardHeader title='Hotel' />
      <CardContent>
        <Grid
          container
          spacing={2}
          justifyContent='center'
          direction='column'
        >
          <Grid item xs={12}>
            <MediaCard src={imgs.beach_1} title={'Beach 1'} />
          </Grid>
          <Grid item xs={12}>
            <MediaCard src={imgs.beach_1} title={'Beach 1'} />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}
