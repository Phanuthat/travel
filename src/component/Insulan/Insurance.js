import {
  Alert,
  AlertTitle,
  Card,
  CardContent,
  CardHeader,
  Grid,
} from '@mui/material';
import React from 'react';

export default function Insurance() {
  return (
    <Card square>
      <CardHeader title='Popular Insurance' />
      <CardContent>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <img
              src='https://www.tqm.co.th/gallery/5866.jpg'
              style={{ height: 230, width: '100%' }}
              alt='Popular-Insurance'
            ></img>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}
