import React from 'react';
import { CarouselStyleWapper } from './CarouselStyle';

export const Carousel = (props) => {
  const settings = {
    dots: true,
    fade: true,
    infinite: true,
    speed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    // prevArrow: null,
    // nextArrow: null,
    arrows: false,
  };
  return (
    <CarouselStyleWapper {...settings} className={props.className}>
      {props.children}
    </CarouselStyleWapper>
  );
};
