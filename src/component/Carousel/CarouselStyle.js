import styled from '@emotion/styled';
import Slider from 'react-slick';
import themeColors from '../../utils/colors';
export const CarouselStyleWapper = styled(Slider)`
  margin-bottom: -7px;
  width: 100%;
  displat: flex;
  padding: 0 auto;
  .slick-dots {
    bottom: 35px;
  }
  .slick-dots li {
    width: 20px;
  }
  .slick-dots li.slick-active button:before {
    color: ${themeColors.primary.main};
  }

  .slick-dots li button:before {
    color: ${themeColors.white};
    opacity: 0.7;
    font-size: 20px;
  }

  @media screen and (max-width: 768px) {
    .slick-dots {
      display: none !important;
    }
  }
`;
