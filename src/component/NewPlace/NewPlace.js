import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Bearch from './Beatch/Bearch';
import Mountain from './Mountain/Mountain';
import Cafe from './Cafe/Cafe';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function NewPlace() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Grid container style={{ position: 'relative', marginTop: 20 }}>
      <div style={{ position: 'absolute' }}>
        <Typography variant='h4' component='div'>
          New Place
        </Typography>
      </div>
      <Box sx={{ width: '100%' }}>
        <Box
          sx={{
            position: 'right',
            display: 'flex',
            justifyContent: 'flex-end',
          }}
        >
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label='basic tabs example'
          >
            <Tab label='Beatch' {...a11yProps(0)} />
            <Tab label='Mountain' {...a11yProps(1)} />
            <Tab label='Cafe' {...a11yProps(2)} />
          </Tabs>
        </Box>
        <TabPanel value={value} index={0}>
          <Bearch />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Mountain />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Cafe />
        </TabPanel>
      </Box>
    </Grid>
  );
}
