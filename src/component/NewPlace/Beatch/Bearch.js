import { Grid } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { getNewBeach, getPopularBeach } from '../../../firestore/beach';
import MediaCard from '../../Card/Card';
import Loading from '../../Loading/Loading';

export default function Bearch() {
  const [newBeachList, setNewBeachList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const call = async () => {
      setIsLoading(true);
      const getNewBeachList = await getNewBeach();
      if (getNewBeachList) {
        setNewBeachList(getNewBeachList);
        setIsLoading(false);
      }
    };
    call();
  }, []);

  return (
    <Grid container spacing={2}>
      {isLoading ? (
        <Loading />
      ) : (
        newBeachList.map((b, i) => {
          return (
            <Grid item xs={4}>
              <MediaCard
                src={b.img}
                title={b.name}
                rate={b.rate}
                location={b.location}
              />
            </Grid>
          );
        })
      )}
    </Grid>
  );
}
