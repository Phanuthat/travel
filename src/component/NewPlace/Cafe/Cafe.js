import { Grid } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { getNewCafe } from '../../../firestore/cafe';
import MediaCard from '../../Card/Card';
import Loading from '../../Loading/Loading';

export default function Cafe() {
  const [newCafeList, setPopularCafeList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const call = async () => {
      setIsLoading(true);
      const getNewCafeList = await getNewCafe();
      if (getNewCafeList) {
        setPopularCafeList(getNewCafeList);
        setIsLoading(false);
      }
    };
    call();
  }, []);

  return (
    <Grid container spacing={2}>
      {isLoading ? (
        <Loading />
      ) : (
        newCafeList.map((b, i) => {
          return (
            <Grid item xs={4}>
              <MediaCard
                src={b.img}
                title={b.name}
                rate={b.rate}
                location={b.location}
              />
            </Grid>
          );
        })
      )}
    </Grid>
  );
}
