import { Grid } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { getNewMountain } from '../../../firestore/mountain';
import MediaCard from '../../Card/Card';
import Loading from '../../Loading/Loading';

export default function Mountain() {
  const [newMountainList, setNewMountainList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const call = async () => {
      setIsLoading(true);
      const getNewMountainKList = await getNewMountain();
      if (getNewMountainKList) {
        setNewMountainList(getNewMountainKList);
        setIsLoading(false);
      }
    };
    call();
  }, []);

  return (
    <Grid container spacing={2}>
      {isLoading ? (
        <Loading />
      ) : (
        newMountainList.map((b, i) => {
          return (
            <Grid item xs={4}>
              <MediaCard
                src={b.img}
                title={b.name}
                rate={b.rate}
                location={b.location}
              />
            </Grid>
          );
        })
      )}
    </Grid>
  );
}
