import React from 'react';
import { makeStyles } from '@mui/styles';
import { imgs } from '../../image/image';
import { Carousel } from '../Carousel/Carousel';

const componentStyles = (theme) => ({
  bannerImg: {
    width: '100%',
    height: '500px',
    objectFit: 'cover',
  },
});

const useStyles = makeStyles(componentStyles);
export default function Banner() {
  const classes = useStyles();
  return (
    <Carousel>
      <img src={imgs.banner} alt='banner' className={classes.bannerImg} />
      <img src={imgs.banner_2} alt='banner' className={classes.bannerImg} />
      <img src={imgs.banner_3} alt='banner' className={classes.bannerImg} />
    </Carousel>
  );
}
