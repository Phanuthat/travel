import { Grid } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { getPopularBeach } from '../../../firestore/beach';
import MediaCard from '../../Card/Card';
import Loading from '../../Loading/Loading';

export default function Bearch() {
  const [popularBeachList, setPopularBeachList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const call = async () => {
      setIsLoading(true);
      const getPopularBeachList = await getPopularBeach();
      if (getPopularBeachList) {
        setPopularBeachList(getPopularBeachList);
        setIsLoading(false);
      }
    };
    call();
  }, []);

  return (
    <Grid container spacing={2}>
      {isLoading ? (
        <Loading />
      ) : (
        popularBeachList.map((b, i) => {
          return (
            <Grid item xs={4}>
              <MediaCard
                src={b.img}
                title={b.name}
                rate={b.rate}
                location={b.location}
              />
            </Grid>
          );
        })
      )}
    </Grid>
  );
}
