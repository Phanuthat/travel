import { Grid } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { getPopularMountain } from '../../../firestore/mountain';
import MediaCard from '../../Card/Card';
import Loading from '../../Loading/Loading';

export default function Mountain() {
  const [popularMountainList, setPopularMountainList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const call = async () => {
      setIsLoading(true);
      const getPopularBeachList = await getPopularMountain();
      if (getPopularBeachList) {
        setPopularMountainList(getPopularBeachList);
        setIsLoading(false);
      }
    };
    call();
  }, []);

  return (
    <Grid container spacing={2}>
      {isLoading ? (
        <Loading />
      ) : (
        popularMountainList.map((b, i) => {
          return (
            <Grid item xs={4}>
              <MediaCard
                src={b.img}
                title={b.name}
                rate={b.rate}
                location={b.location}
              />
            </Grid>
          );
        })
      )}
    </Grid>
  );
}
