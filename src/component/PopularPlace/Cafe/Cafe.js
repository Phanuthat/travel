import { Grid } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { getPopularCafe } from '../../../firestore/cafe';
import MediaCard from '../../Card/Card';
import Loading from '../../Loading/Loading';

export default function Cafe() {
  const [popularCafeList, setPopularCafeList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const call = async () => {
      setIsLoading(true);
      const getPopularCafeList = await getPopularCafe();
      if (getPopularCafeList) {
        setPopularCafeList(getPopularCafeList);
        setIsLoading(false);
      }
    };
    call();
  }, []);

  return (
    <Grid container spacing={2}>
      {isLoading ? (
        <Loading />
      ) : (
        popularCafeList.map((b, i) => {
          return (
            <Grid item xs={4}>
              <MediaCard
                src={b.img}
                title={b.name}
                rate={b.rate}
                location={b.location}
              />
            </Grid>
          );
        })
      )}
    </Grid>
  );
}
