import { createTheme } from '@mui/material/styles';
import themeColors from './utils/colors';
const hexToRgb = (input) => {
  input = input + '';
  input = input.replace('#', '');
  let hexRegex = /[0-9A-Fa-f]/g;
  if (!hexRegex.test(input) || (input.length !== 3 && input.length !== 6)) {
    throw new Error('input is not a valid hex color.');
  }
  if (input.length === 3) {
    let first = input[0];
    let second = input[1];
    let last = input[2];
    input = first + first + second + second + last + last;
  }
  input = input.toUpperCase();
  let first = input[0] + input[1];
  let second = input[2] + input[3];
  let last = input[4] + input[5];
  return (
    parseInt(first, 16) +
    ', ' +
    parseInt(second, 16) +
    ', ' +
    parseInt(last, 16)
  );
};

export const THEME = createTheme({
  typography: {
    fontFamily: ['Roboto', 'sans-serif'].join(','),
    h1: { fontWeight: 'bold' },
    h2: { fontWeight: 'bold' },
    h3: { fontWeight: 'bold' },
    h4: { fontWeight: 'bold' },
    h5: { fontWeight: 'bold' },
    h6: { fontWeight: 'bold' },
  },
  palette: {
    ...themeColors,

    primary: {
      main: '#074973',
    },
    secondary: {
      main: '#eeb737',
    },
    darkblue: {
      main: '#2c2d3d',
    },
    buttonLightLabel: {
      main: 'rgba(' + hexToRgb(themeColors.white.main) + ', 0.95)',
    },
    sidebarLinks: {
      main: 'rgba(' + hexToRgb(themeColors.black.main) + ', 0.5)',
      dark: 'rgba(' + hexToRgb(themeColors.black.main) + ', 0.9)',
    },
    adminNavbarSearch: {
      main: 'rgba(' + hexToRgb(themeColors.white.main) + ', 0.6)',
    },
    authNavbarLink: {
      main: 'rgba(' + hexToRgb(themeColors.white.main) + ', 0.65)',
      dark: 'rgba(' + hexToRgb(themeColors.white.main) + ', 0.95)',
    },
    menuColor: {
      main: themeColors.white.main,
      dark: themeColors.white.main,
    },
  },
});
