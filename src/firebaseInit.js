// Import the functions you need from the SDKs you need
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore'; // TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyCspkkEGGYqPWxiNDXq1XDN8MyouQgsxog',
  authDomain: 'travel-8e834.firebaseapp.com',
  projectId: 'travel-8e834',
  storageBucket: 'travel-8e834.appspot.com',
  messagingSenderId: '728711289010',
  appId: '1:728711289010:web:5054b85a08e1092e0f51ab',
  measurementId: 'G-KNQFL1D6ES',
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

export default db;
