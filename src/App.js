import './App.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { useEffect } from 'react';
import {
  Routes,
  Route,
  Navigate,
  BrowserRouter,
  useLocation,
} from 'react-router-dom';
import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { THEME } from './theme';
import NavBar from './component/AppBar';
import Home from './page/Home';
import Community from './component/Community/Community';

function App() {
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={THEME}>
        <BrowserRouter>
          <NavBar />

          <Routes>
            <Route exact path='/community' element={<Community />} />
            <Route exact path='/' element={<Home />} />
            <Route path='*' element={<Navigate to='/' replace />} />
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </StyledEngineProvider>
  );
}

export default App;
