import db from '../firebaseInit';
import { v4 as uuidv4 } from 'uuid';
export const getPostList = async (limit = 3) => {
  const snapshot = await db.collection('post').limit(limit).get();

  const postList = snapshot.docs.map((doc) => {
    return { ...doc.data(), id: doc.id };
  });
  return postList;
};

export const insertComment = async (id, comments = []) => {
  const snapshot = await db
    .collection('post')
    .doc(id)
    .update({ comments: comments });

  return snapshot;
};
