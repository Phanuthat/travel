import db from '../firebaseInit';

export const getPopularBeach = async (limit = 3) => {
  const snapshot = await db
    .collection('place')
    .where('type', '==', 'beach')
    .where('isPopular', '==', true)
    .limit(limit)
    .get();

  const popularList = snapshot.docs.map((doc) => {
    return doc.data();
  });
  return popularList;
};

export const getNewBeach = async (limit = 3) => {
  const snapshot = await db
    .collection('place')
    .where('type', '==', 'beach')
    .where('isNew', '==', true)
    .limit(limit)
    .get();

  const newList = snapshot.docs.map((doc) => {
    return doc.data();
  });
  return newList;
};
