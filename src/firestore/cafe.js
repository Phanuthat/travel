import db from '../firebaseInit';

export const getPopularCafe = async (limit = 3) => {
  const snapshot = await db
    .collection('place')
    .where('type', '==', 'cafe')
    .where('isPopular', '==', true)
    .limit(limit)
    .get();

  const popularList = snapshot.docs.map((doc) => {
    return doc.data();
  });
  return popularList;
};

export const getNewCafe = async (limit = 3) => {
  const snapshot = await db
    .collection('place')
    .where('type', '==', 'cafe')
    .where('isNew', '==', true)
    .limit(limit)
    .get();

  const newList = snapshot.docs.map((doc) => {
    return doc.data();
  });
  return newList;
};

export const insetCafe = async () => {
  [
    {
      img: 'https://images.unsplash.com/photo-1489528792647-46ec39027556?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
      name: 'my cafe',
      type: 'cafe',
      isNew: false,
      isPopular: true,
      rate: 3,
    },
    {
      img: 'https://images.unsplash.com/photo-1511081692775-05d0f180a065?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=386&q=80',
      name: 'cafe',
      type: 'cafe',
      isNew: false,
      isPopular: true,
      rate: 4,
    },
    {
      img: 'https://images.unsplash.com/photo-1529676468696-f3a47aba7d5d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
      name: 'magic',
      type: 'cafe',
      isNew: false,
      isPopular: true,
      rate: 4,
    },
    {
      img: 'https://images.unsplash.com/photo-1509042239860-f550ce710b93?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
      name: 'toto',
      type: 'cafe',
      isNew: true,
      isPopular: false,
      rate: 3,
    },
    {
      img: 'https://images.unsplash.com/photo-1554118811-1e0d58224f24?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1147&q=80',
      name: 'space',
      type: 'cafe',
      isNew: true,
      isPopular: false,
      rate: 4,
    },
    {
      img: 'https://images.unsplash.com/photo-1508766917616-d22f3f1eea14?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      name: 'กาแฟจ้า',
      type: 'cafe',
      isNew: false,
      isPopular: true,
      rate: 4,
    },
  ].map(async (item) => {
    await db.collection('place').doc().set(item);
  });
};
