import db from '../firebaseInit';

export const getPopularMountain = async (limit = 3) => {
  const snapshot = await db
    .collection('place')
    .where('type', '==', 'mountain')
    .where('isPopular', '==', true)
    .limit(limit)
    .get();

  const popularList = snapshot.docs.map((doc) => {
    return doc.data();
  });
  return popularList;
};

export const getNewMountain = async (limit = 3) => {
  const snapshot = await db
    .collection('place')
    .where('type', '==', 'mountain')
    .where('isNew', '==', true)
    .limit(limit)
    .get();

  const newList = snapshot.docs.map((doc) => {
    return doc.data();
  });
  return newList;
};

export const insetMountain = async () => {
  [
    {
      img: 'https://images.unsplash.com/photo-1633356160600-8374cc708d32?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      name: 'ภูเขาไฟระเบิด',
      type: 'mountain',
      isNew: false,
      isPopular: false,
      rate: 3,
    },
    {
      img: 'https://images.unsplash.com/photo-1641973241002-90b2280f1bfd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=464&q=80',
      name: 'น้ำตกไก่',
      type: 'mountain',
      isNew: false,
      isPopular: true,
      rate: 4,
    },
    {
      img: 'https://images.unsplash.com/photo-1641973240690-9c90ca32cfd4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      name: 'ทุ่งหญ้า',
      type: 'mountain',
      isNew: false,
      isPopular: true,
      rate: 4,
    },
  ].map(async (item) => {
    await db.collection('place').doc().set(item);
  });
};
